# Python Operators are used to perform operations on variables and values.

def addition(a,b):
  total = a + b
  return total

def subtraction(a,b):
    pass


def multiplication(a,b):
    pass


def division(a,b):
    pass

a = 10
b = 5

add_func = (addition(a,b))
sub_func = (subtraction(a,b))
times_func = (multiplication(a,b))
div_func = (division(a,b))

print(f"The value of {a} + {b} is {add_func}")
print(f"The value of {a} - {b} is {sub_func}")
print(f"The value of {a} * {b} is {times_func}")
print(f"The value of {a} / {b} is {div_func}")

# CHALLENGE
# Can you use other Python Operators and if statements to write a function that calculates BMI (bmi = weight / height^2)

# if bmi <= 18.5 return "Underweight"

# if bmi <= 25.0 return "Normal"

# if bmi <= 30.0 return "Overweight"

# if bmi > 30 return "Obese"


